package grails.docker.demo

import grails.plugin.springsecurity.annotation.Secured
import groovy.transform.CompileStatic

@CompileStatic
@Secured('isAuthenticated()')
class HomeController {

    def index() {
        [userName: authenticatedUser?.properties?.get('username')]
    }
}
