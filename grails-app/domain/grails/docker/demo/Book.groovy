package grails.docker.demo

class Book {
    String name
    String author
    Integer size
    String genre
    Boolean isDigitalFormat

    static constraints = {
        name nullable: false, blank: false
        author nullable: false, blank: false
        size nullable: false, min: 3
        genre nullable: false, blank: false, inList: ['horror', 'drama', 'fiction']
        isDigitalFormat nullable: false, blank: false
    }
}
