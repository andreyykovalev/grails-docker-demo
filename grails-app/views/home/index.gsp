<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main" />
    <title>Homepage</title>
</head>

<body>
    <sec:ifLoggedIn>
        <div class="nav" role="navigation">
            <g:set var="entityName" value="Book"/>
            <ul>
                <li><g:link class="list" controller="book" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" controller="book" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <h1>Welcome home, ${userName}</h1>
    </sec:ifLoggedIn>
    <sec:ifNotLoggedIn>
        <h1>Please sign in or sign up to continue</h1>
        <g:link controller="login" action="auth">
            <button class="ui-button" value="Sign in"/>
        </g:link>
    </sec:ifNotLoggedIn>
</body>
</html>